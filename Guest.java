public class Guest {
  public String name;  // the name of the guest
  public String room; // the room the guest booked 

  public Guest(String name, String room){
    this.name = name;
    this.room = room;
  }
}
