# JAVA RMI Coursework

The project contains `6` main java classes and `1` interface. These include


    # HotelServer.java
    # HotelClient.java
    # RoomManagerImpl.java
    # RoomManager.java
    # Guest.java
    # Revenue.java
    # Room.java

The `HotelServer.java` contains code that creates an object from the `RoomManagerImpl` class and binds the created instance to the `/HotelServer` endpoint such that remote clients can remotely call methods of that object when they make appropriate requests to that server.

The `HoteClient.java` contains code that creates an instance of the `RoomManagerImpl` class by calling the `Naming.lookup()` method. It can then access the methods of the remote object through the remote object that is returned. 

The `RoomMangerImpl.java` implements an interface and provides the required data structures to store information about the Hotel Management. The methods implemented access and modify these data structures accordingly to realize the desired functionality

The `RoomManager.java` is an interface that just defines method prototypes that are to be implemented by the remote object .

The `Guest.java` file defines a `Guest` class that defines who a guest is and supports the creation of a Java `ArrayList` to store all guests

The `Revenue.java` file defines a `Revenue` class that defines what a revenue entry is and supports the creation of a Java `ArrayList` to store all revenue entries 

The `Room.java` file defines a `Room` class that defines what a room is and supports the creation of a Java `Array` to store all available rooms 

# Running the project.

To run the project, first you need to start an rmi service using the `rmiregistry` command at a unix shell like so.

    $ rmiregistry

Leave the `rmiregistry` running then complile the `HotelServer.java` like this and run it 

    $ javac HotelServer.java
    $ java HotelServer

You should see the following output.

    😎 Hit me with your requests !!
    Listening ....

Leave the `HotelServer` running and open a new terminal and compile the client and run it 

    $ javac HotelClient.java
    $ java HotelClient

You should see the following output
![](rmi.png)

After you set this up the system is ready to execute some remote methods.

## Here are a few executions and there corresponding output.
#

>`java HotelClient`

![](rmi.png)




>`java HotelClient list rmi://localhost:1099/HotelServer`

![](rmi1.png)

>`java HotelClient book rmi://localhost:1099/HotelServer 1 Bukhupe`

![](rmi2.png)

>`java HotelClient guests rmi://localhost:1099/HotelServer`

![](rmi3.png)

>`java HotelClient revenue rmi://localhost:1099/HotelServer`

![](rmi4.png)