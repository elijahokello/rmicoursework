public class Revenue {
  public String name; // name of person that paid
  public int price; // how much they paid 

  public Revenue(String name, int price){
    this.name = name; 
    this.price = price;
  }
}
