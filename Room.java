public class Room {
  public String type; // room type
  public int number; // the number of avialable rooms 
  public int price; // the price of the room per night 

  public Room(String type, int number, int price ){
    this.type = type;
    this.number = number;
    this.price = price;
  }
}
